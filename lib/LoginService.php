<?php

/**
 * nextcloud - mayfirstauth 
 *
 * @author Jamie McClelland 
 * @copyright 2017 Jamie McClelland <jamie (at) mayfirst (dot) org>
 *
 * Credit and thanks to Andreas Böhler and the authors of the user_sql
 * backend which provided the basis for this app.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\MayfirstAuth;
use Psr\Log\LoggerInterface;

class LoginService extends \OCA\UserExternal\Base {
  private $app_id = NULL;
  private $url = 'https://id.mayfirst.org:8080/';
  private $logger;
  /**
   * The default constructor. Set the $app_id.
   */
  public function __construct($app_id, $url = NULL) {
    $this->app_id = $app_id;
    if ($url) {
      $this->url = $url;
    }
    $this->logger = \OC::$server->get(LoggerInterface::class);
    parent::__construct('mfpl');
  }

  /**
   * Check if the password is correct
   * @param string $uid      The username
   * @param string $password The password
   * @return bool true/false
   *
   */
  public function checkPassword($uid, $password) {
    $uid = strtolower(trim($uid));
    // Only allow users that have been pre-saved to the oc_users_external table
    // via the May First control panel.
    if (!$this->userExists($uid)) {
      $this->logger->error(
        "The user does not exist, add to control panel first.", 
        ['uid' => $uid, 'app' => 'user_external']
      );
      return FALSE;
    }
    $vars = 'user=' . urlencode($uid) . '&password=' . urlencode($password) .
      '&app_id=' . urlencode($this->app_id);
    $ch = curl_init( $this->url . 'check');
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $vars);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec( $ch );

    if ($response == "yes") {
      $this->logger->notice(
        "Check for uid passed.",
        ['uid' => $uid, 'app' => 'user_external']
      );
      // Store the user in the oc_user_external table.
      $this->storeUser($uid);
      return $uid;
    }
    $this->logger->error(
      "Check for uid failed.'.",
      ['uid' => $uid, 'response' => $response, 'app' => 'user_external']
    );
    return FALSE;
  }  

  /**
   * Enables our control panel to insert a user so it's available to be
   * added to a circle before the user actually logs in.
   **/
  public function preSaveUser($user) {
    $this->storeUser($user);
  }
}
?>
