# May First Nextcloud External Authentication application

This app allow usres to login to Nextcloud using their [May
First](https://mayfirst.org/) user name and password.

It depends on the [External User Authentication
app](https://github.com/nextcloud/apps/tree/master/user_external) and will not
work without it.

The login code uses May First's [login
service](https://code.mayfirst.org/mfmt/loginservice).

To use, add the following to your Nextcloud config.php file's $CONFIG array,
replace `$app_id` with a working `app_id` and `$url` with the URL of the 
login service. from the login service:

    'user_backends' => array (
      0 => array (
        'class' => '\OCA\MayfirstAuth\LoginService',
        'arguments' => array (
          0 => $app_id,
          1 => $url,
        ),
      ),
    )
